"""Server untuk aplikasi chat multithread (asynchronous)."""
from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread


def accept_incoming_connections():
    """Mengatur penanganan untuk klien yang masuk."""
    while True:
        client, client_address = SERVER.accept()
        print("%s:%s terhubung." % client_address)
        client.send(bytes("Sekarang ketik nama Anda dan tekan enter!", "utf8"))
        addresses[client] = client_address
        Thread(target=handle_client, args=(client,)).start()


def handle_client(client):  # Mengambil socket klien sebagai argumen.
    """Menangani koneksi klien tunggal."""

    name = client.recv(BUFSIZ).decode("utf8")
    welcome = 'Selamat Datang %s! Jika Anda ingin keluar, ketik {quit} untuk keluar.' % name
    client.send(bytes(welcome, "utf8"))
    msg = "%s telah bergabung dengan chat!" % name
    broadcast(bytes(msg, "utf8"))
    clients[client] = name

    while True:
        msg = client.recv(BUFSIZ)
        if msg != bytes("{quit}", "utf8"):
            broadcast(msg, name+": ")
        else:
            client.send(bytes("{quit}", "utf8"))
            client.close()
            del clients[client]
            broadcast(bytes("%s telah keluar dari chat." % name, "utf8"))
            break


def broadcast(msg, prefix=""):  # prefix untuk identifikasi nama.
    """Broadcasts pesan ke semua klien."""

    for sock in clients:
        sock.send(bytes(prefix, "utf8")+msg)

        
clients = {}
addresses = {}

HOST = ''
PORT = 33000
BUFSIZ = 1024
ADDR = (HOST, PORT)

SERVER = socket(AF_INET, SOCK_STREAM)
SERVER.bind(ADDR)

if __name__ == "__main__":
    SERVER.listen(5)
    print("Menunggu koneksi...")
    ACCEPT_THREAD = Thread(target=accept_incoming_connections)
    ACCEPT_THREAD.start()
    ACCEPT_THREAD.join()
    SERVER.close()